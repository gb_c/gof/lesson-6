#include <iostream>
#include "template_method.h"


int main()
{
    Images* vectorImg = new VectorImage;
    Images* bitmapImg = new BitmapImage;

    vectorImg->process("file1");
    bitmapImg->process("file2");

    return 0;
}
