#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <iostream>


class Calendar
{
private:
    bool weekday;
    bool garbageCollectorDay;

public:
    Calendar(bool wd, bool gc) : weekday(wd), garbageCollectorDay(gc) {}

    bool checkWeekDay()
    {
        return weekday;
    }

    bool checkGarbageCollectorDay()
    {
        return garbageCollectorDay;
    }
};


class Coffee
{
public:
    void doCoffee()
    {
        std::cout << "Starting coffee" << std::endl;
    }
};


class TeaPot
{
public:
    void doTea()
    {
        std::cout << "Starting tea" << std::endl;
    }
};


class Alarm
{
public:
    void doAlarm(std::string& name)
    {
        std::cout << "Rise and shine, " << name << std::endl;
    }
};


class Mediator
{
private:
    Calendar* calend;
    Alarm* alarm;
    Coffee* coffee;
    TeaPot* teaPot;
    std::string nameOwner;

public:
    Mediator(std::string name, Calendar* cal)
        : calend(cal)
        , alarm(new Alarm)
        , coffee(new Coffee)
        , teaPot(new TeaPot)
        , nameOwner(name)
    {}

    ~Mediator()
    {
        delete calend;
        delete alarm;
        delete coffee;
        delete teaPot;
    }

    void doAlarm()
    {
        if(calend->checkGarbageCollectorDay())
        {
            std::cout << "Today is garbage collection day, get up early!" << std::endl;
        }
        else
        {
            if(!calend->checkWeekDay())
            {
                std::cout << "Today is weekend, you can sleep longer!" << std::endl;
            }
        }

        alarm->doAlarm(nameOwner);

        if(calend->checkWeekDay())
            coffee->doCoffee();
        else
            teaPot->doTea();
    }
};

#endif // MEDIATOR_H
