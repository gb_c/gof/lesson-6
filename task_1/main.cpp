#include <iostream>
#include "composite.h"


int main()
{
    CompositeDish* menu = new CompositeDish;

    menu->addDish(sweetPancakes());
    menu->addDish(new PistachioIceCream());

    std::cout << "List menu:" << std::endl << menu->getName() << "Total price is " << menu->getPrice() << std::endl;

    delete menu;

    return 0;
}
