#ifndef COMPOSITE_H
#define COMPOSITE_H

#include <iostream>
#include <vector>


class Dish
{
public:
    virtual float getPrice() = 0;
    virtual std::string getName() = 0;
    virtual ~Dish() {}
};


class ChocolatePancake : public Dish
{
public:
    virtual float getPrice() override
    {
        return 260.0f;
    }

    virtual std::string getName() override
    {
        return "Chocolate pancake";
    }
};


class JamPancake : public Dish
{
public:
    virtual float getPrice() override
    {
        return 190.0f;
    }

    virtual std::string getName() override
    {
        return "Jam pancake";
    }
};


class BananaPancake : public Dish
{
public:
    virtual float getPrice() override
    {
        return 260.0f;
    }

    virtual std::string getName() override
    {
        return "Banana pancake";
    }
};


class PistachioIceCream : public Dish
{
public:
    float getPrice() override
    {
        return 230;
    }

    std::string getName() override
    {
        return "Pistachio ice cream";
    }
};


class CompositeDish : public Dish
{
private:
    std::vector<Dish*> c;

public:
    float getPrice() override
    {
        float total = 0.0f;

        for(auto i : c)
        {
            total += i->getPrice();
        }

        return total;
    }

    std::string getName() override
    {
        std::string list;

        for(auto i : c)
        {
            list += i->getName() + "\n";
        }

        return list;
    }

    virtual void addDish(Dish* p)
    {
        c.push_back(p);
    }

    ~CompositeDish()
    {
        for(auto i : c)
        {
            delete i;
        }
    }
};


CompositeDish* sweetPancakes()
{
    CompositeDish* sp = new CompositeDish;

    sp->addDish(new ChocolatePancake);
    sp->addDish(new BananaPancake);
    sp->addDish(new JamPancake);

    return sp;
}

#endif // COMPOSITE_H
